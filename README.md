关于Gitee Education
-------------
>Gitee Education 是[开源中国社区](http://www.oschina.net)团队开发的高校在线协作平台。

Gitee Education 的功能
-------------
>Gitee 除了提供最基础的 Git 代码托管之外，还提供课程管理、班级/团队管理、实训项目、多种作业类型等功能，具体请查看[帮助](http://git.oschina.net/oschina/git-osc/wikis/帮助)。

意见和建议
-------------
>在您使用 Gitee Education 的过程中有任何意见和建议，请到此项目提交Issue，我们的开发者会在这里跟您沟通。

更新日志
-------------
>Gitee Education 将持续为用户提供更好的代码托管，高校协作等服务，持续发布新特性，想了解 Gitee 近期的新功能可前往 [Gitee 更新日志](http://oschina.gitee.io/git-osc/) 查看。